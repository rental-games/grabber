package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
)

var links = make(chan string, 100)

func main() {
	err := initEnv()
	if err != nil {
		log.Fatal("Can't find some ENV:" + err.Error())
	}
	redisClient, err = ConnectToRedis()
	if err != nil {
		log.Fatal("Can't connect to redis:" + err.Error())
	}
	err = setTitles()
	if err != nil {
		log.Println("(getTitles)Can't checkTitles:" + err.Error())
	}

	end := make(chan bool)
	var pagePCGamerNews PagePCGamerNews
	err = pagePCGamerNews.startParser("www.pcgamer.com", ".article-link", "https://www.pcgamer.com/news/page/")
	if err != nil {
		log.Println("error:", err)
	}
	<-end
}

func setTitles() error {
	url := envVariable("DB_RETURNER") + "/get-news-titels"
	status, body, err := sendGETRequest(url)
	if err != nil {
		return err
	}
	if status != 200 {
		return fmt.Errorf(`url %s returned status %d`, url, status)
	}

	var titles []struct {
		Title string
	}
	_ = json.Unmarshal(body, &titles)

	for _, v := range titles {
		err := SetRedisValue(v.Title, "")
		if err != nil {
			return err
		}
	}
	return nil
}

var envVariableData = map[string]string{
	"DB_RETURNER": "",
	"DIR_LOGOS":   "",
}

func envVariable(key string) string {
	return envVariableData[key]
}

func initEnv() error {
	for k := range envVariableData {
		val, exists := os.LookupEnv(k)
		if !exists || val == "" {
			return fmt.Errorf("error: %s not found", k)
		}
		envVariableData[k] = val
	}
	return nil
}
