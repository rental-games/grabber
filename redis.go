package main

import (
	redis "github.com/go-redis/redis/v7"
)

var redisClient *redis.Client

func ConnectToRedis() (*redis.Client, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	_, err := client.Ping().Result()
	if err != nil {
		return nil, err
	}
	return client, nil
}

func SetRedisValue(key string, value string) error {
	err := redisClient.Set(key, "", 0).Err()
	if err != nil {
		return err
	}
	return nil

}

func CheckRedisKey(key string) (bool, error) {
	_, err := redisClient.Get(key).Result()
	if err != nil {
		if err == redis.Nil {
			return false, nil
		}
		return false, err
	}
	return true, nil
}
