package main

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"

	"github.com/gocolly/colly"
)

type PagePCGamerNews struct {
	Domain      string `json:"-"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Body        string `json:"body"`
	Logo        string `json:"logo"`
	Source      string `json:"source"`
}

func (p *PagePCGamerNews) startParser(domain string, class string, startUrl string) error {
	var count int
	url := startUrl
	for i := 1; i != 1000000; i++ {
		url = startUrl + strconv.Itoa(i) + "/"
		log.Println("url:", url)
		status, err := checkRedirect(url)
		if err != nil {
			log.Println("(checkRedirect)error:", err)
			return err
		}
		switch status {
		case 200:
			p.Domain = domain

			links := make(chan string)
			done := make(chan bool)
			go p.handler(links, done)
			defer func() {
				done <- true
			}()
			c := colly.NewCollector(
				colly.AllowedDomains(p.Domain),
			)
			c.OnHTML(class, func(e *colly.HTMLElement) {
				link := e.Attr("href")
				links <- link

			})

			c.OnRequest(func(r *colly.Request) {
				fmt.Println("Visiting", r.URL.String())
			})

			err := c.Visit(url)
			if err != nil {
				return err
			}
		default:
			count++
			if count > 10 {
				return fmt.Errorf("скраппинг завершен")
			}
		}
	}
	return nil

}

func (p *PagePCGamerNews) handler(msgs chan string, done chan bool) {
	for {
		select {
		case msg := <-msgs:
			p.Source = msg
			p.getContent()
			ok, err := CheckRedisKey(p.Title)
			if err != nil {
				log.Println(fmt.Sprintf("(CheckRedisKey)error: %v", err))
				continue
			}
			if ok {
				//log.Println((fmt.Sprintf("%v уже существует", p.Title)))
				continue
			}
			data, _ := json.Marshal(p)

			status, _, err := sendPOSTRequest(envVariable("DB_RETURNER")+"/save-news", data)
			if err != nil || status != 200 {
				log.Println(fmt.Sprintf("(sendPOSTRequest)error: %v, status: %v", err, status))
				continue
			}
			err = SetRedisValue(p.Title, "")
			if err != nil {
				log.Println((fmt.Sprintf("(SetRedisValue)error:%v", err)))
				continue
			}
			log.Println(fmt.Sprintf("%v успешно сохранен", p.Title))
		case _ = <-done:
			return
		}
	}
}

func (p *PagePCGamerNews) getContent() {
	c := colly.NewCollector(
		// Visit only domains: hackerspaces.org, wiki.hackerspaces.org
		colly.AllowedDomains(p.Domain),
	)
	c.OnHTML(`meta[property]`, func(e *colly.HTMLElement) {
		if e.Attr("property") == `og:title` {
			p.Title = e.Attr("content")
		}
		if e.Attr("property") == `og:description` {
			p.Description = e.Attr("content")
		}
	})

	c.OnHTML(`div[id=article-body]`, func(e *colly.HTMLElement) {
		p.Body = e.ChildText("p")
	})
	ok := false
	c.OnHTML(`img[src]`, func(e *colly.HTMLElement) {
		if e.Attr("class") == ` block-image-ads hero-image` {
			if !ok {
				log.Println(e.Attr(`data-original-mos`))
				fileName := buildFileName(e.Attr(`data-original-mos`))
				file := createFile(fileName)
				putFile(fileName, e.Attr(`data-original-mos`), file, httpClient())
				p.Logo = fileName
				ok = true
			}
		}
	})
	if !ok {
		p.Logo = "image-not-found.jpg"
	}

	c.Visit(p.Source)
}
