package main

import (
	"io"
	"net/http"
	"net/url"
	"os"
	"strings"
)

func putFile(fileName string, fullUrlFile string, file *os.File, client *http.Client) {
	resp, err := client.Get(fullUrlFile)

	checkError(err)

	defer resp.Body.Close()

	_, err = io.Copy(file, resp.Body)

	defer file.Close()

	checkError(err)

	//log.Println(fmt.Sprintf("Just Downloaded a file %s with size %d", fileName, size))
}

func buildFileName(fullUrlFile string) string {
	fileUrl, err := url.Parse(fullUrlFile)
	checkError(err)

	path := fileUrl.Path
	segments := strings.Split(path, "/")

	fileName := segments[len(segments)-1]

	return fileName
}

func httpClient() *http.Client {
	client := http.Client{
		CheckRedirect: func(r *http.Request, via []*http.Request) error {
			r.URL.Opaque = r.URL.Path
			return nil
		},
	}

	return &client
}

func createFile(fileName string) *os.File {
	file, err := os.Create(envVariable(`DIR_LOGOS`) + fileName)

	checkError(err)
	return file
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
